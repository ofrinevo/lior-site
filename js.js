function scroll_to_div(id, e) {
    e.preventDefault()
    document.getElementById(id).scrollIntoView({behavior: 'smooth'});
}

var onscroll = function () {
    var y = window.scrollY;
    if (y >= 200) {
        document.getElementById('back_to_top').className = "sticky visible";
    } else {
        document.getElementById('back_to_top').className = "sticky";
    }
};

window.addEventListener("scroll", onscroll);